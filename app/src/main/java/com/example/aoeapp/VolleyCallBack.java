package com.example.aoeapp;

public interface VolleyCallBack {
    void onSuccess(String result);
}
